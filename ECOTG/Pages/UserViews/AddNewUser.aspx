﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddNewUser.aspx.cs" Inherits="ECOTG.Pages.UserViews.AddNewUser" %>

<!DOCTYPE html>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script src="../../scripts/bootstrap-multiselect.js"></script>
    <%--<script src="../../scripts/mandrill-api.js"></script>--%>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap-multiselect.css">


    <script>
        $(document).ready(function () {
            $(".dropdown-menu").on('click', 'li a', function () {
                $(this).parent().parent().siblings(".btn:first-child").html($(this).text() + ' <span class="caret"></span>');
                $(this).parent().parent().siblings(".btn:first-child").val($(this).text());
            });





            $(function () {
                $('#lstStates').multiselect({
                    buttonText: function (options, select) {
                        //console.log(select[0].length);
                        if (options.length === 0) {
                            return 'None selected';
                        }
                        if (options.length === select[0].length) {
                            return 'All selected (' + select[0].length + ')';
                        }
                        else if (options.length >= 4) {
                            return options.length + ' selected';
                        }
                        else {
                            var labels = [];
                            console.log(options);
                            options.each(function () {
                                labels.push($(this).val());
                            });
                            return labels.join(', ') + '';
                        }
                    }

                });


            });


            var dynamicListItemsForeman = '<li class="list-group-item">' + 'N/A' + '</li>';
            var dynamicListItemsEngineer =
            '<li class="list-group-item">' + 'Action History' + '</li>' +
            '<li class="list-group-item">' + 'Review Claim (Mark as Review)' + '</li>' +
            '<li class="list-group-item">' + 'Edit Claim' + '</li>';


            //Check which Role is selected

            $('#RoleDDL li').on('click', function () {
                //If Foreman selected
                if ($(this).text() == "Foreman") {
                    //Display Foreman's rights in RightsDDL
                    $('#rightsList').html(dynamicListItemsForeman);

                    var data = JSON.parse(jsonData);
                    var ul = document.getElementById("rightsList");
                    /*for( var i = 0; i < data.length; i++ )
                    { 
                      var o = data[i];
                      var li = document.createElement("li");
                      li.appendChild(document.createTextNode(o.rights));
                      ul.appendChild(li);    
            
            
                    }*/
                }
                else if ($(this).text() == "Engineer") {
                    /*var RightsLength = $('#RightsDDL option').size();
                    for(var i=0; i< RightsLength; i++){          
                    }
                    */
                    $('#rightsList').html(dynamicListItemsEngineer);

                }


            });

            $('#addNewRoleBtn').click(function (e) {
                e.preventDefault();
                $('#dynamicNewRole').show();
            })

            $('#cancelAddNewRoleBtn').click(function (e) {
                e.preventDefault();
                $('#dynamicNewRole').hide();
                $('#dynamicNewRight').html('');
            })

            var dynamicNewRoleHTML = '<input type="text" name="" class="form-control">' +
            '<button class="btn "><span class="glyphicon glyphicon-plus"></span></button>';

            $('#addNewRightBtn').click(function (e) {
                e.preventDefault();
                $('#dynamicNewRight').html(dynamicNewRoleHTML);

            })



            /*Generate pw from Mandrill*/
            //var m = new mandrill.Mandrill('23f79cefe571fa7040bd5c4e5b99e007-us14'); // This will be public

            $('#generatePwBtn').click(function (e) {
                e.preventDefault();

                $.ajax({
                    headers: { "Accept": "application/json" },
                    type: "POST",
                    url: "https://mandrillapp.com/api/1.0/messages/send.json",
                    data: {
                        "key": "23f79cefe571fa7040bd5c4e5b99e007-us14",
                        "message": {
                            "from_email": "asterzwx@gmail.com",
                            "to": [
                            {
                                "email": "asterzwx@gmail.com",
                                "name": "aster",
                                "type": "to"
                            }//,

                            ],
                            "autotext": "true",
                            "subject": "YOUR SUBJECT HERE!",
                            "html": "YOUR EMAIL CONTENT HERE! YOU CAN USE HTML!"
                        }
                    }
                }).done(function (response) {
                    console.log(response); // if you're into that sorta thing
                });



                /*  m.messages.send({
                        "message": {
                            "from_email": "asterzwx@gmail.com",
                            "from_name": "your_name",
                            "to":[{"email": "asterzwx@gmail.com", "name": "someone's_name"}], // Array of recipients
                            "subject": "optional_subject_line",
                            "text": "Text to be sent in the body" // Alternatively, use the "html" key to send HTML emails rather than plaintext
                        }
                
                
                });*/

            })


            //$('#confirmBtn').click(function (e) {
            //    e.preventDefault();
            //    alert('created');
            //})



        }); //End of doc ready
    </script>

    <title>New User (System Admin)</title>

    <style>
        .dropdown-menu .sub-menu {
            left: 100%;
            position: absolute;
            top: 0;
            visibility: hidden;
            margin-top: -1px;
        }

        .dropdown-menu li:hover .sub-menu {
            visibility: visible;
        }

        .dropdown:hover .dropdown-menu {
            display: block;
        }
    </style>


</head>

<body>

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="IndexSystemAdmin.aspx">
                    <img src="../../images/ecotg-logo1.png" height="50" width="50" style="margin-top: -15px; margin-left: 15px;">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">

                    <!--  <li class="dropdown">
            <a href="../General Claim Form.html" role="button" aria-haspopup="true" aria-expanded="false">New Claim<span class=""></span></a>
          </li>          
          <li><a href="Index (System Admin).html">My Claim</a></li> -->
                    <li><a href="IndexSystemAdmin.aspx">Users</a></li>
                    <%--<li><a href="Roles Management Index (System Admin).html"> Roles</a></li>--%>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Process Details<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="ProcessDetailsClaimType.aspx">Claim Type</a></li>
                            <li><a href="ProcessDetailsAccountCode.aspx">Account Code</a></li>
                            <li><a href="ProcessDetailsCostCentre.aspx">Cost Centre</a></li>
                            <li><a href="ProcessDetailsCompany.aspx">Company</a></li>
                        </ul>
                    </li>



                </ul>
                <p class="navbar-text navbar-right">
                    Signed in as <a href="#" class="navbar-link">Aster</a> (System Admin)
          <a href='../Login (All).html'><span class='glyphicon glyphicon-log-out'></span></a>
                </p>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>


    <div class="container">

        <div class="panel panel-default">
            <div class="panel-body">

                <!--  <ol class="breadcrumb">
      <li><a href="C:\Users\Donovan\Desktop\SP\FYP\HTML - FYP\Index (Claimant).html">Home</a></li>
      <li>New Refreshment Claim Form</li>
      <li></li>
    </ol> -->
                <h3>Add New User</h3>
                <!-- <span style="float: right; margin-right: 15px">
      <input type="checkbox" checked data-toggle="toggle" data-on="Active" data-onstyle="success"
      data-off="Inactive" data-offstyle="danger">
    </span> -->
                <br>



                <div class="col-md-12">
                    <form name="userForm" id="userForm"  novalidate method="post" runat="server">


                        <!--  <input type="hidden" name="command" value="">
       <input type="hidden" name="isAdmin" value="true"> -->

                        <br>

                        <div class="row">
                            <div class="col-md-5" style="">
                                <div class="form-group">
                                    <label for="fullName">User ID*</label>
                                    <%-- <input type="text" runat="server" name="fullName" class="form-control" id="autoUserId" value="" placeholder="" readonly >--%>
                                    <asp:TextBox ID="autoUserId" runat="server" class="form-control" ReadOnly="true" />
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <%--<input type="text" class="form-control" id="firstName">--%>
                                            <asp:TextBox ID="firstName" runat="server" class="form-control" />
                                        </div>


                                    </div>
                                    <div class="col-md-6">
                                        <label>Last Name</label>
                                        <%--<input type="text" class="form-control" id="lastName">--%>
                                        <asp:TextBox ID="lastName" runat="server" class="form-control" />
                                    </div>
                                </div>

                                <div class="">
                                    <div class="form-group">
                                        <label>NRIC/FIN No.</label>
                                        <input type="text" name="" class="form-control" id="NRIC">
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Date Of Birth*</label>
                                            <input type="date" name="DOB" class="form-control" id="DOB" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Gender*</label>
                                        <br>
                                        <label class="radio-inline">
                                            <input type="radio" name="radioBtn" value="Yes" id="yes">Male</label>
                                        <label class="radio-inline">
                                            <input type="radio" name="radioBtn" value="No" id="no">Female</label>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <strong>Mobile No.</strong><br>
                                        <input type="number" name="" class="form-control" id="mobileNo">
                                    </div>

                                    <div class="col-md-6">
                                        <strong>Home No.</strong><br>
                                        <input type="number" name="" class="form-control" id="homeNo">
                                    </div>

                                </div>

                                <br>
                                <div class="">

                                    <strong>Street Address</strong><br>
                                    <input type="number" name="" class="form-control" id="streetAdd">
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <strong>Unit No.</strong><br>
                                        <input type="number" name="" class="form-control" id="unitNo">
                                    </div>
                                    <div class="col-md-6">
                                        <strong>Postal Code</strong><br>
                                        <input type="number" name="" class="form-control" id="postalCode">
                                    </div>
                                </div>


                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <strong>Bank*</strong>
                                        <div class="form-group">

                                            <div class="btn-group">

                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <span data-bind="label">Select Bank</span>&nbsp;<span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a>DBS/POSB</a></li>
                                                    <li><a>OCBC</a></li>
                                                    <li><a>UOB</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <strong>Account Number</strong><br>
                                        <input type="number" name="accountNumber" class="form-control" id="accountNumber" value="" placeholder="Enter Bank Account No.">
                                    </div>

                                </div>


                                <div class="row">
                                </div>





                                <!--  <button type="submit" name="submit" class="btn btn-success" style="margin-bottom:6px" value="Save" >Submit Claim</button> -->





                                <br>
                                <br>
                            </div>

                            <div class="col-md-1">
                            </div>

                            <div class="col-md-6">

                                <div class="form-group">
                                    <label>Email*</label>
                                    <input type="text" class="form-control" id="email">
                                </div>

                                <div class="form-group">
                                    <label>Password*</label>
                                    <!-- <input type="text" class="form-control" > -->
                                    <br>
                                    <button id="generatePwBtn" class="btn btn-success">Generate Temporary Password</button>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Join Date</label>
                                            <input type="date" class="form-control" id="joinDate">
                                        </div>


                                    </div>
                                    <div class="col-md-6">
                                        <label>End Date</label>
                                        <input type="date" class="form-control" id="endDate">
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <strong>Company</strong><br>
                                        <div class="btn-group">

                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span data-bind="label">Select Company</span>&nbsp;<span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a>Shingda Construction</a></li>
                                                <li><a>Shingda Trading</a></li>
                                                <li><a>Shingda Consultation</a></li>
                                            </ul>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <strong>Department</strong><br>
                                        <div class="btn-group">

                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span data-bind="label">Select Department</span>&nbsp;<span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a>Production</a></li>
                                                <li><a>Finance</a></li>
                                                <li><a>Operation</a></li>
                                                <li><a>IT</a></li>
                                                <li><a>Human Resource</a></li>


                                            </ul>

                                        </div>
                                    </div>

                                </div>
                                <br>


                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="fullName">User Role*</label>
                                        <br>
                                        <div class="btn-group">
                                            <!-- <button type="button"  class="btn btn-default dropdown-toggle" data-toggle="dropdown">
       <span data-bind="label">Select Role</span>&nbsp;<span class="caret"></span>
     </button>
     <ul class="dropdown-menu" role="menu" id="RoleDDL">
       <li><a value="Foreman">Foreman</a></li>
       <li><a value="Engineer">Engineer</a></li>
       <li><a value="SeniorProjectManager">Senior Project Manager</a></li>
     </ul> -->

                                            <!-- <button class="btn"><span class="glyphicon glyphicon-pencil"></span></button> -->

                                            <span id="RightsDDL">
                                                <select size="5" name="lstStates" multiple="multiple" id="lstStates">
                                                    <option value="Foreman">Foreman</option>
                                                    <option value="Engineer">Engineer</option>
                                                    <option value="Senior Project Manager">Senior Project Manager</option>
                                                    <option value="HOD">Head Of Department</option>
                                                    <option value="Account Entry Verifier">Account Entry Verifier</option>
                                                    <option value="Account Payable Verifier">Account Payable Verifier</option>
                                                    <option value="Account Payable Approver">Account Payable Approver</option>
                                                </select>
                                            </span>

                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <!--  <label for="fullName">Rights*</label> -->
                                        <!-- Automated according to selected Role -->
                                        <br>
                                        <!-- Dropdown list -->
                                        <!-- <span id="RightsDDL">
     <select size="5" name="lstStates" multiple="multiple" id="lstStates">
      <option value="Action History">Action History</option>
      <option value="Summary History">Summary History</option>
      <option value="Edit Claim">Edit Claim</option>
      <option value="Review Claim">Review Claim</option>
      <option value="Approve Claim">Approve Claim</option>
      <option value="Create Batch">Create Batch</option>
      <option value="Approve Batch">Approve Batch</option>
      <option value="Update Bank Reference">Update Bank Reference</option>
    </select>  
  </span>  -->

                                        <!-- ListGroup, <li> items should be dynamic -->
                                        <!-- <ul class="list-group" id="rightsList">
    
    <li class="list-group-item">____ </li>
  </ul> -->

                                        <br>
                                        <div class="btn-group">

                                            <!--  <button type="button" class=" btn btn-default dropdown-toggle" data-toggle="dropdown">
       <span data-bind="label">Select Right(s)</span>&nbsp;<span class="caret"></span>
     </button>
     <ul class="dropdown-menu" role="menu">
       <li><a><input type="checkbox" name=""/>&nbsp;View Action History</a></li> 
       <li><a><input type="checkbox" name=""/>&nbsp;View Summary History</a></li>                
       <li><a><input type="checkbox" name=""/>&nbsp;Edit Claim</a></li>           
       <li><a><input type="checkbox" name=""/>&nbsp;Review Claim</a></li>
       <li><a><input type="checkbox" name=""/>&nbsp;Approve Claim</a></li>
       <li><a><input type="checkbox" name=""/>&nbsp;Reject Claim</a></li>
       <li><a><input type="checkbox" name=""/>&nbsp;Create Batch</a></li>         
       <li><a><input type="checkbox" name=""/>&nbsp;Approve Batch</a></li>
       <li><a><input type="checkbox" name=""/>&nbsp;Update Bank Reference</a></li>


     </ul> -->
                                            <br>
                                        </div>
                                    </div>




                                </div>

                                <!-- <div>
  <button class="btn btn-primary" id="addNewRoleBtn">Add New Role</button>
</div> -->

                                <br>
                                <div class="row" id="dynamicNewRole" hidden="true">
                                    <div class="col-md-6">
                                        <label>New Role</label>
                                        <input type="text" name="" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label>New Right</label>
                                        <input type="text" name="" class="form-control">
                                        <button class="btn" id="addNewRightBtn"><span class="glyphicon glyphicon-plus"></span></button>
                                        <span id="dynamicNewRight">
                                            <!-- Dynamically add input box and add button here -->
                                            <!-- <input type="text" name="" class="form-control">
    <button class="btn "><span class="glyphicon glyphicon-plus"></span></button> -->

                                        </span>
                                    </div>
                                    <div class="col-md-6">
                                        <button class="btn btn-primary">Save New Role</button>
                                        <button class="btn" id="cancelAddNewRoleBtn">Cancel</button>
                                    </div>
                                </div>


                            </div>

                        </div>
                        <div style="text-align: center;">
                            <%--<button type="submit" name="submit" class="btn btn-success" style="margin-bottom: 20px; position: block;" value="Save" id="confirmBtn">Confirm</button>--%>
                             <asp:LinkButton OnClick="ConfirmBtn_Clicked" runat="server" ID="confirmBtn" CssClass="btn btn-success" CausesValidation="false" AutoPostBack="false">
                                <span class="" aria-hidden="true" ></span>
                                Confirm
                            </asp:LinkButton>
                            <button type="" name="" class="btn btn-default" style="margin-bottom: 20px; margin-left: 20px; position: block;"
                                value="Save">
                                Cancel</button>
                            <br />
                           

                        </div>

                    </form>

                </div>
            </div>
        </div>


    </div>


</body>
</html>



