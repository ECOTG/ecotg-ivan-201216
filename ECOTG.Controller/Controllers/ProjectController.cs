﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Data;
using ECOTG.Model.Entity;

namespace ECOTG.Controller.Controllers
{
    public static class ProjectController
    {
        public static readonly FormCrudWithStringKeyService<project> FormCurdService;

        static ProjectController()
        {
            FormCurdService = new FormCrudWithStringKeyService<project>(new ProjectEntity());
        }
    }
}
