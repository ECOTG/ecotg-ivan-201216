﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Data;
using ECOTG.Model.Entity;

namespace ECOTG.Controller.Controllers
{
    public static class AsetController
    {
        public static readonly FormCrudWithStringKeyService<asset> FormCurdService;

        static AsetController()
        {
            FormCurdService = new FormCrudWithStringKeyService<asset>(new AssetEntity());
        }
    }
}
