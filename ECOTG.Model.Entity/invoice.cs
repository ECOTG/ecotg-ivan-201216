//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ECOTG.Model.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class invoice
    {
        public invoice()
        {
            this.lineitem = new HashSet<lineitem>();
        }
    
        public int invoiceId { get; set; }
        public string invoiceNo { get; set; }
        public Nullable<System.DateTime> startDate { get; set; }
        public Nullable<System.DateTime> endDate { get; set; }
        public string claimId { get; set; }
    
        public virtual claim claim { get; set; }
        public virtual ICollection<lineitem> lineitem { get; set; }
    }
}
