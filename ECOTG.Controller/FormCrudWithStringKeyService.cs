﻿using System;
using ECOTG.Model.Data;
using ECOTG.Helper;


namespace ECOTG.Controller
{
    public class FormCrudWithStringKeyService<T> where T : new()
    {
        private readonly IEntityCrudWithStringKey<T> _crud;

        public FormCrudWithStringKeyService(IEntityCrudWithStringKey<T> crud)
        {
            _crud = crud;
        }

        public T GetEntity(string id)
        {
            return _crud.GetEntity(id);
        }

        //public string BindEntities(RadGrid grid)
        //{
        //    try
        //    {
        //        grid.DataSource = _crud.GetAllEntities();
        //    }
        //    catch (Exception ex)
        //    {
        //        return ExceptionHelper.GetFriendlyMessageForException(ex);
        //    }

        //    return string.Empty;
        //}

        public string Add(T entity, bool isNewable)
        {
            if (isNewable)
            {
                try
                {
                    _crud.Insert(entity);
                    return MessageFormatHelper.GetFormattedSuccessMessage(MessageHelper.Successful.Added);
                }
                catch (Exception ex)
                {
                    return ExceptionHelper.GetFriendlyMessageForException(ex);
                }
            }

            return MessageFormatHelper.GetFormattedNoticeMessage(MessageHelper.AccessDenined.Adding);
        }

        public string Edit(T entity, bool isEditable)
        {
            if (isEditable)
            {
                try
                {
                    _crud.Update(entity);

                    return MessageFormatHelper.GetFormattedSuccessMessage(MessageHelper.Successful.Updated);
                }
                catch (Exception ex)
                {
                    return ExceptionHelper.GetFriendlyMessageForException((ex));
                }
            }

            return MessageFormatHelper.GetFormattedNoticeMessage(MessageHelper.AccessDenined.Updating);
        }

        public string Remove(string id, bool isDeletable)
        {
            if (isDeletable)
            {
                try
                {
                    var entity = _crud.GetEntity(id);

                    _crud.Delete(entity);

                    return MessageFormatHelper.GetFormattedSuccessMessage(MessageHelper.Successful.Deleted);
                }
                catch (Exception ex)
                {
                    return ExceptionHelper.GetFriendlyMessageForException((ex));
                }
            }

            return MessageFormatHelper.GetFormattedNoticeMessage(MessageHelper.AccessDenined.Deleting);
        }


    }
}
