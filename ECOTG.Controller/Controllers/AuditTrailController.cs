﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Data;
using ECOTG.Model.Entity;

namespace ECOTG.Controller.Controllers
{
    public static class AuditTrailController
    {
        public static readonly FormCrudWithIntegerKeyService<audittrail> FormcurdService;

        static AuditTrailController()
        {
            FormcurdService = new FormCrudWithIntegerKeyService<audittrail>(new AuditTrailEntity());
        }
    }
}
