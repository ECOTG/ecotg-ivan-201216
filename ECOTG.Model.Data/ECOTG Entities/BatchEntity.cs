﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class BatchEntity : IEntityCrudWithStringKey<batch>
    {
        public batch GetEntity(string id)
        {
            using (var db = new ecotgEntities())
            {
                return db.batch.FirstOrDefault(e => e.batchId == id);
            }
        }

        public IEnumerable<batch> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.batch select e;
                return entities.ToList();
            }
        }

        public void Insert(batch entity)
        {
            using (var db = new ecotgEntities())
            {
                db.batch.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(batch entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(batch entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.batch.Remove(entity);
            }
        }
    }
}
