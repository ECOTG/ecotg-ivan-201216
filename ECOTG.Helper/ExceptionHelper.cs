﻿using System;
using System.Data.Common;
using System.IO;
using System.Web;

namespace ECOTG.Helper
{
    public static class ExceptionHelper
    {
        public static string GetFriendlyMessageAndWriteErrorLog(Exception exception, object source)
        {
            try
            {
                WriteErrorLog(exception, source);
            }
            catch (Exception ex)
            {
                throw new HttpUnhandledException(string.Format("Exception Message: {0}| Exception Source: {1}", ex.Message, source.ToString()), exception);
            }
            finally
            {
                NotifySystemAdmin(exception);
            }

            return GetFriendlyMessageForException(exception);
        }

        public static string GetPlainFriendlyMessageForException(Exception exception)
        {
            string message = string.Empty;

            Exception rootException = exception;
            Exception innerException = exception.InnerException;

            while (innerException != null)
            {
                rootException = innerException;
                innerException = innerException.InnerException;
            }

            if (rootException is DbException)
            {
                message = string.Format("Database operation problem occurred. {0}", rootException.Message);
            }
            else if (rootException is NullReferenceException)
            {
                message = string.Format("There are one or more required fields that are missing. {0}", rootException.Message);
            }
            else
            {
                var argumentException = rootException as ArgumentException;

                if (argumentException != null)
                {
                    string paramName = argumentException.ParamName;

                    message = string.Concat("The ", paramName, " value is invalid.");
                }
                else if (rootException is ApplicationException)
                {
                    message = string.Format("Exception occur in application. {0}", rootException.Message);
                }
                else
                {
                    message = rootException.Message;
                }
            }

            return message;
        }

        public static string GetFriendlyMessageForException(Exception exception)
        {
            string message = GetPlainFriendlyMessageForException(exception);

            return MessageFormatHelper.GetFormattedErrorMessage(message);
        }

        public static void WriteErrorLog(Exception exception, object source)
        {
            StreamWriter logStream = null;

            string fileName = string.Format("~/_ErrorLog/{0}{1}", DateTime.Now.ToString("ddMMyyyyhhmmss"), ".txt");
            string logFilePath = HttpContext.Current.Server.MapPath(fileName);

            try
            {
                logStream = new StreamWriter(logFilePath, true);
                logStream.WriteLine("********** Source:[{0}] **********", source.ToString());
                logStream.Write("Exception Type: ");
                logStream.WriteLine(exception.GetType().ToString());
                logStream.WriteLine("Exception: " + exception.Message);
                logStream.WriteLine("Stack Trace: ");

                if (exception.StackTrace != null)
                {
                    logStream.WriteLine(exception.StackTrace);
                    logStream.WriteLine();
                }

                Exception temp = exception.InnerException;
                int count = 1;

                while (temp != null)
                {
                    logStream.Write("Inner Exception Type {0}: ", count);
                    logStream.WriteLine(temp.InnerException.GetType().ToString());

                    logStream.Write("Inner Exception {0}: ", count);
                    logStream.WriteLine(temp.InnerException.Message);

                    logStream.Write("Inner Source {0}: ", count);
                    logStream.WriteLine(temp.InnerException.Source);

                    if (temp.StackTrace != null)
                    {
                        logStream.WriteLine("Inner Stack Trace {0}: ", count);
                        logStream.WriteLine(temp.StackTrace);
                    }

                    temp = temp.InnerException;
                    count++;
                }
            }
            catch (Exception ex)
            {
                throw new HttpUnhandledException(string.Format("Exception Message: {0}| Exception Source: {1}", ex.Message, source.ToString()), exception);
            }
            finally
            {
                if (logStream != null)
                {
                    logStream.Close();
                }
            }

        }

        public static void NotifySystemAdmin(Exception exception)
        {
            throw new NotImplementedException();
        }
    }
}
