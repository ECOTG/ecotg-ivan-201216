//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ECOTG.Model.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class refreshment
    {
        public int refreshmentId { get; set; }
        public Nullable<int> engineerId { get; set; }
        public Nullable<System.DateTime> startDate { get; set; }
        public Nullable<System.DateTime> endDate { get; set; }
        public Nullable<decimal> totalAmount { get; set; }
        public Nullable<int> projectManagerId { get; set; }
        public Nullable<int> hodId { get; set; }
        public string claimId { get; set; }
    
        public virtual claim claim { get; set; }
        public virtual refreshmentdetails refreshmentdetails { get; set; }
    }
}
