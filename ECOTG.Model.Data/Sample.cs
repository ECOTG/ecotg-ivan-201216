﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    // Basic CRUD operations
//    public class SampleEntity :  IEntityCrudWithIntegerKey<*Table name in entity diagram example: lib_Catalog*>
//    {

//        public lib_Catalogs GetEntity(int id)
//        {
//            using (var db = new SGLIBEntities())
//            {
//                return db.lib_Catalogs.FirstOrDefault(e => e.CatalogID == id);
//            }
//        }

//        public IEnumerable<lib_Catalogs> GetAllEntities()
//        {
//            using (var db = new SGLIBEntities())
//            {
//                var entities = from e in db.lib_Catalogs
//                               select e;

//                return entities.ToList();
//            }
//        }

//        public void Insert(lib_Catalogs entity)
//        {
//            using (var db = new SGLIBEntities())
//            {
//                db.lib_Catalogs.Add(entity);
//                db.SaveChanges();
//            }
//        }

//        public void Update(lib_Catalogs entity)
//        {
//            using (var db = new SGLIBEntities())
//            {
//                db.Entry(entity).State = EntityState.Modified;
//                db.SaveChanges();
//            }
//        }

//        public void Delete(lib_Catalogs entity)
//        {
//            using (var db = new SGLIBEntities())
//            {
//                db.Entry(entity).State = EntityState.Deleted;
//                db.lib_Catalogs.Remove(entity);
//                db.SaveChanges();
//            }
//        }

//    }
}
