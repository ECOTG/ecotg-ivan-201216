﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IndexSystemAdmin.aspx.cs" Inherits="ECOTG.View.WebUI.Pages.UserViews.IndexSystemAdmin" %>

<!DOCTYPE html>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">


    <script>
        $(document).ready(function(){
            $('#myTable').DataTable({
            });
        });
    </script>


    <style>
    .dropdown-menu .sub-menu {
    left: 100%;
    position: absolute;
    top: 0;
    visibility: hidden;
    margin-top: -1px;
  }

  .dropdown-menu li:hover .sub-menu {
    visibility: visible;
  }
  .dropdown:hover .dropdown-menu {
    display: block;
  }
        
    </style>
   

  <title>Index</title>
</head>
<body>

    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="IndexSystemAdmin.aspx">
          <img src="../../images/ecotg-logo1.png" height="50" width="50" style="margin-top: -15px; margin-left: 15px;">
        
        </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
<!-- 
        <li class="dropdown">
          <a href="../General Claim Form.html" role="button" aria-haspopup="true" aria-expanded="false">New Claim<span class=""></span></a>
         
    </li>

    <li><a href="Index (System Admin).html">My Claim</a></li> -->
    <li><a href="IndexSystemAdmin.aspx"> Users</a></li>
    <%--<li><a href="Roles Management Index (System Admin).html"> Roles</a></li>--%>
    <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Process Details<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="ProcessDetailsClaimType.aspx">Claim Type</a></li>
              <li><a href="ProcessDetailsAccountCode.aspx">Account Code</a></li>
                <li><a href="ProcessDetailsCostCentre.aspx">Cost Centre</a></li>
                <li><a href="ProcessDetailsCompany.aspx">Company</a></li>
            </ul>
          </li>



</ul>
<p class="navbar-text navbar-right">Signed in as <a href="#" class="navbar-link">Aster</a> (System Admin)
 <a href='../Login (All).html'><span class='glyphicon glyphicon-log-out'></span></a>
</p>
</div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>
<div class="container">
<div class="panel panel-default">
     <div class="panel-body">
        <h1>Users</h1>
        <a class="btn btn-success" type="button" href="" style="float:right" onclick="alert('Imported')">
        Import <span class="badge"></span>
        </a>
        
        
        <a class="btn btn-primary" type="button" href="AddNewUser.aspx" style="margin-right:10px; float:right">
        Add New User <span class="badge"></span>
        </a>

        <br><br>
       <!--  <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">Current Claim</h3>
            </div>
            <div class="panel-body">



                <table id="myTable" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Claim Form ID</th>
                        <th>Status</th>
                        <th>Date of Submission</th>
                        <th>Reimbursment Status</th>
                        <th>Full Report</th>
                        
                    </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>RE0004</td>
                    <td style="color:green">Approved</td>
                    <td>15 April 2016</td>
                    <td>Reimbursed: Bank Reference Number #3029482</td>
                    <td><a class="btn btn-primary" role="button" href="../General Claim Form.html" style="background-color:green";>
                        View<span class="badge"></span>
                    </a></td>
                </tr>
           
              <tr>
                <td>RE0005</td>
                <td style="color:red">Rejected</td>
                <td>30 April 2016</td>
                <td></td>
                <td><a class="btn btn-primary" role="button" href="../General Claim Form.html" style="background-color:green";>
                    View<span class="badge"></span>
                    
                </a>
                <a class="btn btn-primary" role="button" href="../General Claim Form.html";>
                 <span class="glyphicon glyphicon-repeat"></span><span class="badge"></span>

             </a></td>
         </tr>
    
      <tr>
        <td>RE0006</td>
        <td style="color:blue">Pending</td>
        <td>20 Feb 2016</td>
        <td></td>
        <td><a class="btn btn-primary" role="button" href="../General Claim Form.html" style="background-color:green";>
            View<span class="badge"></span>
        </a></td>
    </tr>
</tbody>

</table>


</div>
</div> -->



<!--  --><!--  --><!--  -->

          



                <table id="myTable" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Email</th>
                        <th>No. of Role(s)</th> <!-- No. of rights a user has -->
                        <th>Manage</th>
                        
                    </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1234</td>
                    <td>John Doe</td>
                    <td>Foreman</td>
                    <td>johndoedoe88@shingda.com</td>
                    <td>1</td>
                    <td>
                        <a class="btn btn-primary" role="button" href="Add New User (System Admin).html">
                            Edit<span class="badge"></span>                            
                        </a>
                        <a class="btn btn-danger" role="button" onclick="alert('Deleted')">
                            <span class="glyphicon glyphicon-remove" ;"></span>                            
                        </a>

                    </td>
                </tr>

                <tr>
                    <td>2237</td>
                    <td>Rachel Lim Mei Feng</td>
                    <td>Account Entry Verifier</td>
                    <td>rachel_lmf90@shingda.com</td>
                    <td>2</td>
                    <td>
                        <a class="btn btn-primary" role="button" href="Add New User (System Admin).html">
                            Edit<span class="badge"></span>                            
                        </a>
                        <a class="btn btn-danger" role="button" onclick="alert('Deleted')">
                            <span class="glyphicon glyphicon-remove"></span>                            
                        </a>

                    </td>
                </tr>

                <tr>
                   <td>4879</td>
                   <td>Manjit Sutaria</td>
                   <td>Engineer</td>
                   <td>Manjit_Sutaria85@shingda.com</td>
                   <td>1</td>
                   <td>
                    <a class="btn btn-primary" role="button" href="Add New User (System Admin).html">
                        Edit<span class="badge"></span>                            
                    </a>
                    <a class="btn btn-danger" role="button" onclick="alert('Deleted')">
                        <span class="glyphicon glyphicon-remove"></span>                            
                    </a>

                </td>
            </tr>
            <tr>
                   <td>5509</td>
                   <td>Nadheera Saah</td>
                   <td>Account Payable Verifier</td>
                   <td>nadsaah8989@shingda.com</td>
                   <td>1</td>
                   <td>
                    <a class="btn btn-primary" role="button" href="Add New User (System Admin).html">
                        Edit<span class="badge"></span>                            
                    </a>
                    <a class="btn btn-danger" role="button" onclick="alert('Deleted')">
                        <span class="glyphicon glyphicon-remove"></span>                            
                    </a>

                </td>
            </tr>
            <tr>
                   <td>3667</td>
                   <td>Thomas Harris</td>
                   <td>Director</td>
                   <td>thomas_harris80@shingda.com</td>
                   <td>1</td>
                   <td>
                    <a class="btn btn-primary" role="button" href="Add New User (System Admin).html">
                        Edit<span class="badge"></span>                            
                    </a>
                    <a class="btn btn-danger" role="button" onclick="alert('Deleted')">
                        <span class="glyphicon glyphicon-remove"></span>                            
                    </a>

                </td>
            </tr>
        </tbody>

    </table>


</div>
</div>



</div>
</body>
</html>

