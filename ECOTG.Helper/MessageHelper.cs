﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOTG.Helper
{
    public class MessageHelper
    {
        public struct Successful
        {
            public static string Added = "Record(s) added successful.";
            public static string Updated = "Record(s) edited successful.";
            public static string Deleted = "Record(s) deleted successful.";

            public static string Applied = "Application applied successful.";
            public static string Saved = "Record(s) saved successful.";
            public static string Removed = "Record(s) removed successful.";

            public static string FileUploaded = "File uploaded successful.";
            public static string FileDeleted = "File deleted successful.";
        }

        public struct AccessDenined
        {
            public static string Action = "You don't have permission for this action.";
            public static string Viewing = "You don't have permission for view the record.";
            public static string Adding = "You don't have permission for create the new record.";
            public static string Updating = "You don't have permission for editing the record.";
            public static string Deleting = "You don't have permission for delete the record.";

            public static string Removing = "You don't have permission for remove the record.";

            public static string Printing = "You don't have permission for print record(s).";
            public static string Exporting = "You don't have permission for export record(s).";
        }

        public struct General
        {
            public static string ObjectNotFound = "Object not found";
            public static string NoRowSelected = "No row has been selected";
        }


    }
}
