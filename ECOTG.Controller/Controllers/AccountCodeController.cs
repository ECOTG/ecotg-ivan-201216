﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Data;
using ECOTG.Model.Entity;

namespace ECOTG.Controller.Controllers
{
    public static class AccountCodeController
    {
        public static readonly FormCrudWithStringKeyService<accountcode> FormCurdService;

        static AccountCodeController()
        {
            FormCurdService = new FormCrudWithStringKeyService<accountcode>(new AccountCodeEntity());
        }
    }
}
