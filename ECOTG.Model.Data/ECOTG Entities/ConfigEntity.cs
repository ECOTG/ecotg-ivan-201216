﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class ConfigEntity : IEntityCrudWithIntegerKey<config>
    {
        public config GetEntity(int id)
        {
            using (var db = new ecotgEntities())
            {
                return db.config.FirstOrDefault(e => e.configId == id);
            }
        }

        public IEnumerable<config> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.config select e;
                return entities.ToList();
            }
        }

        public void Insert(config entity)
        {
            using (var db = new ecotgEntities())
            {
                db.config.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(config entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(config entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.config.Remove(entity);
            }
        }
    }
}
