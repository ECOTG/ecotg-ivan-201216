﻿namespace ECOTG.Helper
{
    public static class MessageFormatHelper
    {
        public static string GetFormattedRemindMessage(string message)
        {
            return GetFormattedMessage(message, AppEnum.MessageType.Remind);
        }

        public static string GetFormattedSuccessMessage(string message)
        {
            return GetFormattedMessage(message, AppEnum.MessageType.Success);
        }

        public static string GetFormattedErrorMessage(string message)
        {
            return GetFormattedMessage(message, AppEnum.MessageType.Error);
        }

        public static string GetFormattedNoticeMessage(string message)
        {
            return GetFormattedMessage(message, AppEnum.MessageType.Notice);
        }

        public static string GetFormattedMessage(string message, AppEnum.MessageType messageType)
        {
            switch (messageType)
            {
                case AppEnum.MessageType.Success:
                    return "<div class='success'>" + message + "</div>";
                case AppEnum.MessageType.Error:
                    return "<div class='error'>" + message + "</div>";
                case AppEnum.MessageType.Notice:
                    return "<div class='notice'>" + message + "</div>";
                case AppEnum.MessageType.Remind:
                    return "<div class='remind'>" + message + "</div>";
                default:
                    return "<div class='normal'>" + message + "</div>";
            }
        }

    }

}
