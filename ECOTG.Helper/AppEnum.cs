﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOTG.Helper
{
    public struct AppEnum
    {
        public enum MessageType
        {
            Success,
            Error,
            Notice,
            Remind
        }

    }
}
