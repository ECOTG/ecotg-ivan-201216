﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Data;
using ECOTG.Model.Entity;

namespace ECOTG.Controller.Controllers
{
    public static class UserController
    {
        public static readonly FormCrudWithStringKeyService<user> FormCurdService;

        static UserController()
        {
            FormCurdService = new FormCrudWithStringKeyService<user>(new UserEntity());
        }

    }
}
