﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Data;
using ECOTG.Model.Entity;

namespace ECOTG.Controller.Controllers
{
    public static class RefreshmentController
    {
        public static readonly FormCrudWithIntegerKeyService<refreshment> FormCurdService;

        static RefreshmentController()
        {
            FormCurdService = new FormCrudWithIntegerKeyService<refreshment>(new RefreshmentEntity());
        }
    }
}
