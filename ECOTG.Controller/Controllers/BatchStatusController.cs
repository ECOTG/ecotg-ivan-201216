﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Data;
using ECOTG.Model.Entity;

namespace ECOTG.Controller.Controllers
{
    public static class BatchStatusController
    {
        public static readonly FormCrudWithIntegerKeyService<batchstatus> FormCurdService;

        static BatchStatusController()
        {
            FormCurdService = new FormCrudWithIntegerKeyService<batchstatus>(new BatchStatusEntity());
        }
    }

    
}
