﻿namespace ECOTG.Model.Data
{
    public interface IEntityCrudWithIntegerKey<T> : IEntityCrud<T>
    {
        T GetEntity(int id);
    }
}
