﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ECOTG.Model.Entity;

namespace ECOTG.Model.Data
{
    public class RoleEntity : IEntityCrudWithIntegerKey<role>
    {
        public role GetEntity(int id)
        {
            using (var db = new ecotgEntities())
            {
                return db.role.FirstOrDefault(e => e.roleId == id);
            }
        }

        public IEnumerable<role> GetAllEntities()
        {
            using (var db = new ecotgEntities())
            {
                var entities = from e in db.role select e;
                return entities.ToList();
            }
        }

        public void Insert(role entity)
        {
            using (var db = new ecotgEntities())
            {
                db.role.Add(entity);
                db.SaveChanges();
            }
        }

        public void Update(role entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(role entity)
        {
            using (var db = new ecotgEntities())
            {
                db.Entry(entity).State = EntityState.Deleted;
                db.role.Remove(entity);
            }
        }
    }
}
