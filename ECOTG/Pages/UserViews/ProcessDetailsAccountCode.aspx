﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProcessDetailsAccountCode.aspx.cs" Inherits="ECOTG.View.WebUI.Pages.UserViews.ProcessDetailsAccountCode" %>

<!DOCTYPE html>
<html>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <script src="../js/bootstrap-multiselect.js"></script>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

  <link rel="stylesheet" type="text/css" href="../css/bootstrap-multiselect.css">

  <script>
    $(document).ready(function(){
      $('#financeTable').DataTable({
      });

      $('#projectTable').DataTable({
      });

      $('#HRTable').DataTable({
      });



      $("#tableTabs a").click(function(e){
        e.preventDefault();
        $(this).tab('show');
        var activeTab =  $('.nav-tabs .active > a').attr('href');
        console.log(activeTab);
        
        
      });

     /* var defaultModalBody = '<div class="row">' +
      '<div class="col-md-6">' +
      '<label id="dynamicRoleLabel"> </label> <br>' +
      '<input type="text" name="" class="form-control"> </div>' +
      '<div class="col-md-6">   <label id="dynamicRightLabel"> </label>   <br>' +
      '<input type="text" name="" class="form-control"> ' +
      '<button id="addNewRightBtn" type="button" class="btn">' + 
      '<span class="glyphicon glyphicon-plus"></span></button>' +
      '<div id="dynamicNewRight">  </div> </div>  </div>';

      $('#modalBody').html(defaultModalBody);

      var defaultModalTitle = "Add New Role";
      var defaultRoleLabel = "New Role";
      var defaultRightLabel = "Role Above";

      var editModalTitle = "Edit Role";
      var editRoleLabel = "Role";
      var editRightLabel = "Right(s)";

      $('#mymodal').click(function(e){
        e.preventDefault();
        $('#myModalLabel').html(defaultModalTitle);
        $('#dynamicRoleLabel').html(defaultRoleLabel);
        $('#dynamicRightLabel').html(defaultRightLabel);

      })
      $('#editRoleBtn').click(function(e){
        e.preventDefault();
        $('#myModalLabel').html(editModalTitle);
        $('#dynamicRoleLabel').html(editRoleLabel);
        $('#dynamicRightLabel').html(editRightLabel);
      })*/

      var defaultModalTitle = "Add New Role";
      var addFinanceRole = "Add New Finance Role";
      var addProjectRole = "Add New Project Role";

      var editModalTitle = "Edit Role";

     /* $('#addFinanceRoleBtn').click(function(e){
        $('#myModalLabel').html(addFinanceRole);
      })
*/
      $('#addProjectRoleBtn').click(function(e){
        $('#myModalLabel').html(addProjectRole);
      })



      $('#editRoleBtn').click(function(e){
       $('#myModalLabel').html(editModalTitle);
     })




     /* var dynamicNewRoleHTML = '<input type="text" name="" class="form-control">' +
      '<button class="btn "><span class="glyphicon glyphicon-plus"></span></button>' +
      '<button class="btn btn-danger" id="removeRoleInputBtn"><span class="glyphicon glyphicon-minus"></span></button>';*/

      

      /*Dynamic minus button doesnt work*/

      $('#addNewRightBtn').click(function(e){
        e.preventDefault();
        $('#dynamicNewRight').html(dynamicNewRoleHTML);

      })

      $('#removeRoleInputBtn').click(function(e){
        e.preventDefault();
        $('#dynamicNewRight').html('');

      })

      
      //Confirm delete popup
     /* $('#removeRoleBtn').click(function(e){
        e.preventDefault();
        var confirmDialog = confirm("Confirm delete?");
        if (confirmDialog == true) {
          alert("You have deleted Role");
        } else {
          alert("Cancelled");
        }
      })*/


      $("#roleBelowDDL").on('click', 'li a', function(){
        $(this).parent().parent().siblings(".btn:first-child").html($(this).text()+' <span class="caret"></span>');
        $(this).parent().parent().siblings(".btn:first-child").val($(this).text());
      })

      $("#roleAboveDDL").on('click', 'li a', function(){
        $(this).parent().parent().siblings(".btn:first-child").html($(this).text()+' <span class="caret"></span>');
        $(this).parent().parent().siblings(".btn:first-child").val($(this).text());
      })



      $(function () { 
        $('#lstStates').multiselect({ 
          buttonText: function(options, select) {
            console.log(select[0].length);
            if (options.length === 0) {
              return 'None selected';
            }
            if (options.length === select[0].length) {
              return 'All selected ('+select[0].length+')';
            }
            else if (options.length >= 4) {
              return options.length + ' selected';
            }
            else {
              var labels = [];
              console.log(options);
              options.each(function() {
                labels.push($(this).val());
              });
              return labels.join(', ') + '';
            }
          }

        });


      });

      $(function () { 
        $('#roleBelowList').multiselect({ 
          buttonText: function(options, select) {
            console.log(select[0].length);
            if (options.length === 0) {
              return 'None selected';
            }
            if (options.length === select[0].length) {
              return 'All selected ('+select[0].length+')';
            }
            else if (options.length >= 4) {
              return options.length + ' selected';
            }
            else {
              var labels = [];
              console.log(options);
              options.each(function() {
                labels.push($(this).val());
              });
              return labels.join(', ') + '';
            }
          }

        });


      });





      $('#closeModalBtn').click(function(e){
        e.preventDefault();
        //$('#dynamicNewRight').html('');
        $('#lstStates option').prop('selected', function() {
          return this.defaultSelected;
        });

      })



    }); //End of doc ready
  </script>


  <style>
   .dropdown-menu .sub-menu {
    left: 100%;
    position: absolute;
    top: 0;
    visibility: hidden;
    margin-top: -1px;
  }

  .dropdown-menu li:hover .sub-menu {
    visibility: visible;
  }
  .dropdown:hover .dropdown-menu {
    display: block;
  }
  
</style>


<title>Account Code Mgmt</title>
</head>
<body>

  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="IndexSystemAdmin.aspx">
          <img src="../../images/ecotg-logo1.png" height="50" width="50" style="margin-top: -15px; margin-left: 15px;">
        
        </a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">

          <!-- <li class="dropdown">
            <a href="../General Claim Form.html" role="button" aria-haspopup="true" aria-expanded="false">New Claim<span class=""></span></a>

          </li>

          <li><a href="Index (System Admin).html">My Claim</a></li> -->
          <li><a href="Index (System Admin).html"> Users</a></li>
          <%--<li><a href="Roles Management Index (System Admin).html"> Roles</a></li>--%>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Process Details<span class="caret"></span></a>
            <ul class="dropdown-menu">
            <li><a href="ProcessDetailsClaimType.aspx">Claim Type</a></li>
              <li><a href="ProcessDetailsAccountCode.aspx">Account Code</a></li>
                <li><a href="ProcessDetailsCostCentre.aspx">Cost Centre</a></li>
                <li><a href="ProcessDetailsCompany.aspx">Company</a></li>
            </ul>
          </li>          



        </ul>
        <p class="navbar-text navbar-right">Signed in as <a href="#" class="navbar-link">Aster</a> (System Admin)
         <a href='../Login (All).html'><span class='glyphicon glyphicon-log-out'></span></a>
       </p>
     </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
 </nav>

 <div class="container">


  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Add New Account Code</h4>
        </div>
        <div class="modal-body" id="modalBody" style="height: 100px;">

          <div class="row">
            <div class="col-md-6">
              <label>Account Code</label>
              <input type="text" name="" class="form-control">
            </div>
            <div class="col-md-6">

            </div>

          </div>
          <br>
          <div class="row">
           

           </div>



              <!-- <div class="row">
                <div class="col-md-6">
                  <label id="dynamicRoleLabel"> </label>
                  <br>
                  <input type="text" name="" class="form-control">
                </div>
                <div class="col-md-6">
                  <label id="dynamicRightLabel"> </label>
                  <br>
                  <input type="text" name="" class="form-control"> 
                  <button id="addNewRightBtn" type="button" class="btn"><span class="glyphicon glyphicon-plus"></span></button>
                  <div id="dynamicNewRight">   
                                 

                  </div>
                </div>
              </div> -->

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" id="closeModalBtn">Close</button>
              <button type="button" class="btn btn-primary" data-dismiss="modal" id="saveModalBtn">Save changes</button>
            </div>
          </div>
        </div>
      </div>
      <!-- End of Modal -->
    <div class="panel panel-default">
     <div class="panel-body">


      <h1>Account Code</h1>





         <button id="addFinanceRoleBtn" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"
         style="float:right">
         Add New Code <span class="badge"></span>
       </button> <!-- Trigger Modal -->
       <br><br>

       <table id="financeTable" class="table table-bordered" >
        <thead>
          <tr>
            <th>ID</th>
            <th>Type</th>


            <th>Manage</th>

          </tr>
        </thead>
        <tbody>

          <tr>
           <td>000-5000-00</td>
           <td>Transportation</td>

           <td>
            <a class="btn btn-primary" role="button" data-toggle="modal" data-target="#myModal">
              Edit<span class="badge"></span>                            
            </a>
            <a class="btn btn-danger" role="button" onclick="alert('Deleted')">
              <span class="glyphicon glyphicon-remove"></span>                            
            </a>

          </td>
        </tr>
        <tr>
         <td>000-5100-00</td>
         <td>Entertainment</td>

         <td>
          <a class="btn btn-primary" role="button" data-toggle="modal" data-target="#myModal">
            Edit<span class="badge"></span>                            
          </a>
          <a class="btn btn-danger" role="button" onclick="alert('Deleted')">
            <span class="glyphicon glyphicon-remove"></span>                            
          </a>

        </td>
      </tr>
      <tr>
       <td>000-5500-00</td>
       <td>Misc</td>

       <td>
        <a class="btn btn-primary" role="button" data-toggle="modal" data-target="#myModal">
          Edit<span class="badge"></span>                            
        </a>
        <a class="btn btn-danger" role="button" onclick="alert('Deleted')">
          <span class="glyphicon glyphicon-remove"></span>                            
        </a>

      </td>
    </tr>
   
 
</tbody>

</table>


</div>
</div>
</div>
</body>
</html>

