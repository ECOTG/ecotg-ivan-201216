﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Data;
using ECOTG.Model.Entity;

namespace ECOTG.Controller.Controllers
{
    public static class RefreshmentDetailController
    {
        public static readonly FormCrudWithIntegerKeyService<refreshmentdetails> FormCurdService;

        static RefreshmentDetailController()
        {
            FormCurdService = new FormCrudWithIntegerKeyService<refreshmentdetails>(new refreshmentdetailsDetailsEntity());
        }
    }
}
