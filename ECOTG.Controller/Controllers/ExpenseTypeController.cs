﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECOTG.Model.Data;
using ECOTG.Model.Entity;

namespace ECOTG.Controller.Controllers
{
    public static class ExpenseTypeController
    {
        public static readonly FormCrudWithIntegerKeyService<expensetype> FormCurdService;

        static ExpenseTypeController()
        {
            FormCurdService = new FormCrudWithIntegerKeyService<expensetype>(new ExpenseTypeEntity());
        }
    }
}
